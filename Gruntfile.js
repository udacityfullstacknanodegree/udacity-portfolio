module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    bower: {
      install: {
        options: {
          targetDir: 'src/',
          cleanTargetDir: false,
          verbose: true,
          layout: function(type, component, source) {
              return type;
          },
        }
      }
    },
    csslint: {
      strict: {
        options: {
          "box-model": 1,
          "display-property-grouping": 2,
          "duplicate-properties": 2,
          "empty-rules": 2,
          "known-properties": 2,
          "adjoining-classes": 0
        },
        src: ['src/css/main*.css']
      }
    },
    jshint: {
      all: ['Gruntfile.js', 'src/js/scripts.js']
    },
    bootlint: {
      options: {
        relaxerror: [],
        showallerrors: true,
        stoponerror: true,
        stoponwarning: true
      },
      files: ['src/*.html']
    },
    useminPrepare: {
      html: 'dist/index.html'
    },
    usemin: {
      html: 'dist/index.html'
    },
    copy: {
      dist: {
        cwd: 'src',
        src: ['app.yaml', '*.html', 'img/*.*', 'fonts/*.*'],
        dest: 'dist',
        expand: true
      },
    },
    concat: {
      csslib: {
        dest: '.tmp/concat/css/lib.min.css',
        src: [
          'src/css/bootstrap.min.css'
        ]
      },
      jslib: {
        dest: '.tmp/concat/js/lib.min.js',
        src: [
          'src/js/jquery.min.js',
          'src/js/bootstrap.min.js'
        ]
      },
      css: {
        dest: '.tmp/concat/css/style.css',
        src: [
          'src/css/main.css',
          'src/css/main-w970.css'
        ]
      },
      js: {
        dest: '.tmp/concat/js/scripts.js',
        src: [
          'src/js/scripts.js'
        ]
      },
      jsconcat: {
        dest: 'dist/js/app.js',
        src: [
          '<%= concat.jslib.dest %>',
          '.tmp/min/js/scripts.min.js'
        ]
      },
      cssconcat: {
        dest: 'dist/css/app.css',
        src: [
          '<%= concat.csslib.dest %>',
          '.tmp/min/css/style.min.css'
        ]
      }
    },
    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer')({browsers: ['last 2 version']})
        ]
      },
      files: {
        src: '<%= concat.css.dest %>'
      }
    },
    uglify: {
      dist: {
        files: {
          '.tmp/min/js/scripts.min.js':'<%= concat.js.dest %>'
        }
      }
    },
    cssmin: {
      dist: {
        files: {
          '.tmp/min/css/style.min.css':'<%= concat.css.dest %>'
        }
      }
    },
    clean: {
      dist: {
        src: ['dist']
      },
      tmp: {
        src: ['.tmp']
      },
      img: {
        src: ['src/img/*-w*.{jpg,gif,png}']
      }
    },
    respimg: {
      default: {
        options: {
          optimize: false,
          widths: [480,960,1440]
        },
        files: [{
          expand: true,
          cwd: 'src/img/',
          src: ['*.{gif,jpg,png,svg}', '!*-w*.{gif,jpg,png,svg}'],
          dest: 'src/img/'
        }]
      }
    },
    connect: {
      prod: {
        options: {
          keepalive: true,
          open: true,
          port: 9001,
          base: 'dist'
        }
      },
      dev: {
        options: {
          livereload: true,
          open: true,
          port: 9000,
          base: 'src'
        }
      }
    },
    watch: {
      js: {
        files: ['src/js/scripts.js'],
        tasks: ['jshint'],
        options: {
          livereload: true,
        },
      },
      css: {
        files: ['src/css/main*.css'],
        tasks: ['csslint'],
        options: {
          livereload: true,
        },
      },
      html: {
        files: ['src/*.html'],
        tasks: ['bootlint'],
        options: {
          livereload: true,
        },
      },
    }
  });

  grunt.registerTask('check', [
    'jshint',
    'csslint',
    'bootlint'
  ]);

   grunt.registerTask('dev', [
     'check',
     'connect:dev',
     'watch'
   ]);

  grunt.registerTask('buildcss', [
    'concat:csslib',
    'concat:css',
    'postcss',
    'cssmin:dist',
    'concat:cssconcat',
  ]);

  grunt.registerTask('buildjs', [
    'concat:jslib',
    'concat:js',
    'uglify:dist',
    'concat:jsconcat',
  ]);

  grunt.registerTask('build', [
    'check',
    'clean:dist',
    'copy:dist',
    'useminPrepare',
    'buildcss',
    'buildjs',
    'usemin'
  ]);

  // grunt.registerTask('prod', [
  //   'build',
  //   'connect:prod',
  // ]);
};
