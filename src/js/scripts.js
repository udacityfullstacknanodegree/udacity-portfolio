$('.carousel').carousel({
  interval: false
});

$( ".tags .label" ).click(function() {
  // Get tag
  var tag = "."+$(this).attr("class").split(" ")[1];
  if ($(this).hasClass("active") === true) {
    // Unselect tag and show all projects
    $(".portfolio").find(tag).removeClass("active");
    $(".project").parent().show(1000);
  } else {
    // Select tag and show only projects having this tag
    $(".portfolio").find(".label").removeClass("active");
    $(".portfolio").find(tag).addClass("active");
    $(".project:has("+tag+")").parent().show(1000);
    $(".project").not(":has("+tag+")").parent().hide(1000);
  }
});
